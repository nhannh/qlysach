/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Process;

import Database.Connect;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author nhannh
 */
public class muontra {
    public Connect cn = new Connect();
    public ResultSet xemmt() throws SQLException{
        cn.connectSQL();
        String sql = "select sophieu,tens,tentv,ngaymuon,hantra,ngaytra,a.mas,a.matv from muontra a,sach b,thanhvien c where a.mas=b.mas and a.matv=c.matv";
        return cn.Loaddata(sql);
    }
    public ResultSet tongmuon() throws SQLException{
        cn.connectSQL();
        String sql="select count(*) as tongmuon from muontra";
        return cn.Loaddata(sql);
    }
    public ResultSet xemmt(String mas) throws SQLException{
        String sql="select sophieu,tens,tentv,ngaymuon,hantra,ngaytra,a.mas,a.matv from muontra a,sach b,thanhvien c where a.mas=b.mas and a.matv=c.matv and sophieu='"+mas+"'";
        return cn.Loaddata(sql);
    }
    public void xoamt(String mas) throws SQLException{
        String sql ="delete from muontra where sophieu='"+mas+"'";
        cn.Updatedata(sql);
    }
    public void themmt(String mas, String tensach, String malv, String manxb,String matg,String soluong) throws SQLException{
        String sql ="insert into muontra values ('"+mas+"','"+tensach+"','"+malv+"','"+manxb+"','"+matg+"','"+soluong+"')";
        cn.Updatedata(sql);
    }
    public void suamt(String mas, String tensach, String malv, String manxb,String matg,String soluong) throws SQLException{
        String sql ="update muontra set matv='"+tensach+"',mas='"+malv+ "',ngaymuon='"+manxb+"',hantra='"+matg+"',ngaytra='"+soluong+ "' where sophieu='"+mas+"'";
        cn.Updatedata(sql);
    }
    public void trasach(String mas,String soluong) throws SQLException{
        String sql ="update muontra set ngaytra='"+mas+ "' where sophieu='"+soluong+"'";
        cn.Updatedata(sql);
    }
    public ResultSet timnhieutv(String tentimkiem) throws SQLException{
        String sql="select sophieu,tens,tentv,ngaymuon,hantra,ngaytra,a.mas,a.matv from muontra a, sach b,thanhvien c where a.mas=b.mas and a.matv=c.matv and (tentv like N'%"+tentimkiem+"%' or tens like N'%"+tentimkiem+"%')";
        return cn.Loaddata(sql);
    }
}
