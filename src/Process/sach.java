/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Process;

import Database.Connect;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author nhannh
 */
public class sach {
    public Connect cn = new Connect();
    public ResultSet xemsach() throws SQLException{
        cn.connectSQL();
        String sql = "select mas,tens,a.matg,tentg,a.manxb, tennxb,a.malv,tenlv,soluong from Sach a,Tacgia b,NhaXuatBan c,linhvuc d where a.malv=d.malv and a.manxb=c.manxb and a.matg=b.matg";
        return cn.Loaddata(sql);
    }
    public ResultSet xemms(String mas) throws SQLException{
        String sql="select mas,tens,a.matg,tentg,a.manxb,tennxb,a.malv,tenlv,soluong from Sach a,Tacgia b,NhaXuatBan c,linhvuc d where a.malv=d.malv and a.manxb=c.manxb and a.matg=b.matg and mas='"+mas+"'";
        return cn.Loaddata(sql);
    }
    public ResultSet tongsach() throws SQLException{
        cn.connectSQL();
        String sql="select count(*) as tongsach from sach";
        return cn.Loaddata(sql);
    }
    public ResultSet tongsl() throws SQLException{
        cn.connectSQL();
        String sql="select sum(soluong) as slsach from sach";
        return cn.Loaddata(sql);
    }
    
    public ResultSet laycbms(String tennxb) throws SQLException{
        String sql="select mas from sach where tens=N'"+tennxb+"'";
        return cn.Loaddata(sql);
    }
    public void xoas(String mas) throws SQLException{
        String sql ="delete from sach where mas='"+mas+"'";
        cn.Updatedata(sql);
    }
    public void thems(String mas, String tensach, String malv, String manxb,String matg,int soluong) throws SQLException{
        String sql ="insert into sach values ('" +mas +"',N'" +tensach+"','"+malv+"','"+manxb+"','"+matg+"',"+soluong+")";
        cn.Updatedata(sql);
    }
    public void suas(String mas, String tensach, String malv, String manxb,String matg,int soluong) throws SQLException{
        String sql ="update sach set tens=N'"+tensach+"',malv='"+malv+ "',manxb='"+manxb+"',matg='"+matg+"',soluong="+soluong+ " where mas='"+mas+"'";
        cn.Updatedata(sql);
    }
    public ResultSet tims(String muctimkiem, String tentimkiem) throws SQLException{
        String sql="select mas,tens,a.matg,tentg,a.manxb,tennxb,a.malv,tenlv,soluong from Sach a,Tacgia b,NhaXuatBan c,linhvuc d where a.malv=d.malv and a.manxb=c.manxb and a.matg=b.matg and "+muctimkiem+" like N'%"+tentimkiem+"%'";
        return cn.Loaddata(sql);
    }
    public ResultSet timnhieus(String tentimkiem) throws SQLException{
        String sql="select mas,tens,a.matg,tentg,a.manxb,tennxb,a.malv,tenlv,soluong from Sach a,Tacgia b,NhaXuatBan c,linhvuc d where a.malv=d.malv and a.manxb=c.manxb and a.matg=b.matg and (tens like N'%"+tentimkiem+"%' or tentg like N'%"+tentimkiem+"%' or tennxb like N'%"+tentimkiem+"%' or tenlv like N'"+tentimkiem+"%')";
        return cn.Loaddata(sql);
    }
    
}
