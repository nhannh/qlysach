
package Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import  javax.swing.*;


public class Connect {
    public  static Connection conn;
    public static String user ="nhan1";
    public  static String pass = "123456";
    public  static String url = "jdbc:sqlserver://localhost:1433;databaseName = QLS;";
    public static String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public void connectSQL() throws SQLException{
        try {
           
        //  String url = "jdbc:sqlserver://localhost:1433;databaseName = QLS;";
           Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
           conn = java.sql.DriverManager.getConnection(url, user, pass);
           
            
        } catch (ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null,"ket noi CSDL that bai","Thong bao",1);
        }
    }
    public ResultSet Loaddata(String sql){
        try {
            Statement statement = conn.createStatement();
            return statement.executeQuery(sql);
            
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
    public void Updatedata(String sql){
        try {
            Statement statement = conn.createStatement();
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public static boolean open()
    {
        try
        {
            if (conn == null || conn.isClosed())
            {
                Class.forName(driver);
                conn = DriverManager.getConnection(url, user, pass);
            }
            return true;
        }
        catch (SQLException ex)
        {
            System.out.println(ex.getMessage()+"1");
        }
        catch (ClassNotFoundException ex)
        {
            System.out.println(ex.getMessage()+"2");
        }
        return false;
    }

    public static void close()
    {
        try
        {
            if (conn != null)
                conn.close();
        } 
        catch (SQLException ex)
        {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    
    public static void close(PreparedStatement ps)
    {
        try
        {
            if (ps != null)
                ps.close();
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        close();
    }
    
    public static void close(PreparedStatement ps, ResultSet rs)
    {
        try
        {
            if (rs !=null)
                rs.close();
        }
        catch (SQLException ex)
        {
            Logger.getLogger(Connect.class.getName()).log(Level.SEVERE, null, ex);
        }
        close(ps);
    }

    
}
