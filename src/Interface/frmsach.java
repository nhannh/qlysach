
package Interface;
import java.sql.*;
import javax.swing.*;
import Process.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;



public final class frmsach extends javax.swing.JFrame {
    private final sach ms = new sach();
    private final nxb nhaxb = new nxb();
    private final linhvuc lv = new linhvuc();
    private final tacgia tg = new tacgia();
    private final DefaultTableModel tableModel = new DefaultTableModel();
    private boolean cothem = true;
    public void showdata() throws  SQLException{
        ResultSet result = ms.xemsach();
        try {
            while (result.next()) {
                String row[] =new String[6];
                row[0] = result.getString(1);
                row[1] = result.getString(2);
                row[2] = result.getString(4);
                row[3] = result.getString(6);
                row[4] = result.getString(8);
                row[5] = result.getString(9);
             
                tableModel.addRow(row);
                
            }
            
        } catch (SQLException e) {
        }
    }
    public final void ShowDataCombonxb() {         
        ResultSet result=null;                   
        try { 
                result = nhaxb.xemnxb();                        
                while(result.next()){  
                cbonhaxuatban.addItem(result.getString("tennxb")); 
            } 
        }   
        catch (SQLException e) { 
        }
    }
    public final void ShowDataCombolv() {         
        ResultSet result=null;                   
        try { 
                result = lv.xemlv();                        
                while(result.next()){  
                cbotl.addItem(result.getString("tenlv")); 
            } 
        }   
        catch (SQLException e) { 
        }
    }
    public final void ShowDataCombotg() {         
        ResultSet result=null;                   
        try { 
                result = tg.xemtg();                        
                while(result.next()){  
                cbotg.addItem(result.getString("tentg")); 
            } 
        }   
        catch (SQLException e) { 
        }
    }
    public void ShowTentg(String tentg) throws SQLException
    {                 
        ResultSet result =  tg.xemtg(tentg);           
        if(result.next()){ 
            // nếu còn đọc tiếp được một dòng dữ liệu            
        txttacgia.setText(result.getString("matg")); 
        }         
    }   
    public void ShowTennxb(String tennxb) throws SQLException
    {                 
        ResultSet result =  nhaxb.xemnxb(tennxb);
        if(result.next()){ 
            // nếu còn đọc tiếp được một dòng dữ liệu            
        txtnhaxuatban.setText(result.getString("manxb")); 
        }         
    }   
    public void ShowTenlv(String tenlv) throws SQLException
    {                 
        ResultSet result =  lv.xemlv(tenlv);
        if(result.next()){ 
            // nếu còn đọc tiếp được một dòng dữ liệu            
        txtthloai.setText(result.getString("malv")); 
        }         
    }   
    
    public void cleardata() throws SQLException{
        int n = tableModel.getRowCount()-1;
        for (int i = n; i >=0; i--) {
            tableModel.removeRow(i);        
        }        
    }
    private void setnull(){   
        txtmasach.setText(null);
        txtnhaxuatban.setText(null);
        txtsoluong.setText(null);
        txttacgia.setText(null);
        txttensach.setText(null);
        txtthloai.setText(null);
    //    cbonhaxuatban.setSelectedItem("");
      //  cbotg.setSelectedItem(null);
    //    cbotl.setSelectedItem(null);
        
    }
    private void setkhoa(boolean a){
        txtmasach.setEnabled(!a);
        txtnhaxuatban.setEnabled(!a);
        txtsoluong.setEnabled(!a);
        txttacgia.setEnabled(!a);
        txttensach.setEnabled(!a);
        txtthloai.setEnabled(!a);
        cbonhaxuatban.setEnabled(!a);
        cbotg.setEnabled(!a);
        cbotl.setEnabled(!a);
    }
    private void setbutton(boolean a){
        btnthem.setEnabled(a);
        btnxoa.setEnabled(a);
        btnsua.setEnabled(a);
        btnluu.setEnabled(!a);
        btnkluu.setEnabled(!a);
        
        
    }
    
    public frmsach() throws SQLException {
        initComponents();
        String []colname = {"Mã sách","Tên sách","Tên tác giả","Tên nhà xuất bản","Tên thể loại","Số lượng"};
        tableModel.setColumnIdentifiers(colname);
        tblsach.setModel(tableModel);
        showdata();
        ShowDataCombonxb();
        ShowDataCombolv();
        ShowDataCombotg();
        setnull();
        setkhoa(true);
        setbutton(true);
    
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtmasach = new javax.swing.JTextField();
        txttensach = new javax.swing.JTextField();
        cbotg = new javax.swing.JComboBox<>();
        txttacgia = new javax.swing.JTextField();
        cbonhaxuatban = new javax.swing.JComboBox<>();
        txtnhaxuatban = new javax.swing.JTextField();
        cbotl = new javax.swing.JComboBox<>();
        txtthloai = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtsoluong = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblsach = new javax.swing.JTable();
        btnthem = new javax.swing.JButton();
        btnxoa = new javax.swing.JButton();
        btnsua = new javax.swing.JButton();
        btnluu = new javax.swing.JButton();
        btnkluu = new javax.swing.JButton();
        btnthoat = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Danh mục sách");

        jPanel1.setBackground(new java.awt.Color(34, 167, 240));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("QUẢN LÝ DANH MỤC SÁCH");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(302, 302, 302)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(589, 589, 589))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin sách", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Mã sách:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Tên sách:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Thể loại:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("Tác giả");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Nhà xuất bản:");

        cbotg.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbotgItemStateChanged(evt);
            }
        });

        cbonhaxuatban.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbonhaxuatbanItemStateChanged(evt);
            }
        });

        cbotl.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbotlItemStateChanged(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Số lượng:");

        tblsach.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Mã sách", "Tên sách", "Mã nxb", "Tên NXB", "Mã TG", "Tên TG", "Mã TL", "Tên TL", "Số lượng"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, false, true, false, true, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblsach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblsachMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblsach);

        btnthem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Add.png"))); // NOI18N
        btnthem.setText("Thêm");
        btnthem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnthemActionPerformed(evt);
            }
        });

        btnxoa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Delete.png"))); // NOI18N
        btnxoa.setText("Xóa");
        btnxoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnxoaActionPerformed(evt);
            }
        });

        btnsua.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Edit.png"))); // NOI18N
        btnsua.setText("Sửa");
        btnsua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsuaActionPerformed(evt);
            }
        });

        btnluu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Save.png"))); // NOI18N
        btnluu.setText("Lưu");
        btnluu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnluuActionPerformed(evt);
            }
        });

        btnkluu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/reload.png"))); // NOI18N
        btnkluu.setText("K.lưu");
        btnkluu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnkluuActionPerformed(evt);
            }
        });

        btnthoat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Exit.png"))); // NOI18N
        btnthoat.setText("Thoát");
        btnthoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnthoatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel4)
                            .addComponent(jLabel7))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(cbonhaxuatban, 0, 83, Short.MAX_VALUE)
                                    .addComponent(cbotg, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtnhaxuatban, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txttacgia, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(27, 27, 27))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtsoluong, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(cbotl, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtthloai, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(25, 25, 25))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtmasach, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txttensach, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(102, 102, 102)
                .addComponent(btnthem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnxoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnsua)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnluu)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnkluu)
                .addGap(18, 18, 18)
                .addComponent(btnthoat)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 250, Short.MAX_VALUE)
                        .addGap(43, 43, 43))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtmasach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txttensach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(cbotg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txttacgia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(cbonhaxuatban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtnhaxuatban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(cbotl, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtthloai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtsoluong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnthem)
                    .addComponent(btnxoa)
                    .addComponent(btnsua)
                    .addComponent(btnluu)
                    .addComponent(btnkluu)
                    .addComponent(btnthoat))
                .addGap(67, 67, 67))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tblsachMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblsachMouseClicked
         try {
            int row = tblsach.getSelectedRow();
            String mas = (tblsach.getModel().getValueAt(row, 0)).toString();
            ResultSet rs = ms.xemms(mas);
            if (rs.next()) {
                txtmasach.setText(rs.getString("MaS"));
                txtnhaxuatban.setText(rs.getString("Manxb"));
                txtsoluong.setText(rs.getString("soluong"));
                txttacgia.setText(rs.getString("matg"));
                txttensach.setText(rs.getString("tens"));
                txtthloai.setText(rs.getString("malv"));
                cbonhaxuatban.setSelectedItem(rs.getString("tennxb"));
                cbotl.setSelectedItem(rs.getString("tenlv"));
                cbotg.setSelectedItem(rs.getString("tentg"));
                
               
            }
            
        } catch (SQLException e) {
        }
    }//GEN-LAST:event_tblsachMouseClicked

    private void btnthemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnthemActionPerformed
        setnull();
        setbutton(false);
        setkhoa(false);
        cothem =true;
    }//GEN-LAST:event_btnthemActionPerformed

    private void btnxoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnxoaActionPerformed
        String msv = txtmasach.getText();
        String tensach = txttensach.getText();
        try {
            if (msv.length()==0) {
                JOptionPane.showMessageDialog(null, "Chọn 1 sản phẩm để xóa", "Thông báo", 1);
            }
            else {
                if(JOptionPane.showConfirmDialog(null,"Bạn có chắc chắn muốn xóa sách '"+ tensach +"' này hay không?" ,"Thông báo",2)==0)
                {
                    ms.xoas(msv);
                    cleardata();
                    showdata();
                    setnull();
                }
            }       
        } catch (SQLException e) {
        }
    }//GEN-LAST:event_btnxoaActionPerformed

    private void btnsuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsuaActionPerformed
        String mas = txtmasach.getText();
        if (mas.length()==0) {
            JOptionPane.showMessageDialog(null, "Chọn mã sách cần sửa","Thông báo",1);
        }
        else{
            setkhoa(false);
            txtmasach.setEnabled(false);
            setbutton(false);
            cothem = false;
        }
                      
    }//GEN-LAST:event_btnsuaActionPerformed

    private void btnluuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnluuActionPerformed
        String ma=txtmasach.getText();         
        String ten=txttensach.getText();
        String matl=txtthloai.getText(); 
        String manxb=txtnhaxuatban.getText(); 
        String matg=txttacgia.getText(); 
        int soluong =  Integer.parseInt(txtsoluong.getText());
        if(ma.length()==0 || ten.length()==0)              
                JOptionPane.showMessageDialog(null,"Vui long nhap Ma Sach va ten sach", "Thong bao",1);         
        else 
         //   if(ma.length()>0 || ten.length()>0)              
             //   JOptionPane.showMessageDialog(null,"Ma SP chi 2 ky tu, ten SP la 30", "Thong bao",1);             
        //else                
        {               
            try 
            { 
                if(cothem==true)    
                //Luu cho tthem moi      
                  
                ms.thems(ma, ten, matl, manxb, matg, soluong);
                else                
                //Luu cho sua 
                ms.suas(ma, ten, matl, manxb, matg, soluong);
                cleardata();
                showdata();
            } 
                catch (SQLException ex) 
                { 
                   JOptionPane.showMessageDialog(null,"Cap nhat that bai","Thong bao",1); 
                }                         
                    setnull();
                    setkhoa(true);             
                    setbutton(true); 
        }

    }//GEN-LAST:event_btnluuActionPerformed

    private void btnkluuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnkluuActionPerformed
        setnull();
        setkhoa(true);
        setbutton(true);
    }//GEN-LAST:event_btnkluuActionPerformed

    private void btnthoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnthoatActionPerformed
        this.dispose();
        frmMain f = new frmMain();
        f.setVisible(true);
    }//GEN-LAST:event_btnthoatActionPerformed

    private void cbotgItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbotgItemStateChanged
   
        try {             
                String tentg=cbotg.getSelectedItem().toString();             
                ShowTentg(tentg);
         } 
         catch (SQLException ex) {             
             Logger.getLogger(frmsach.class.getName()).log(Level.SEVERE, null, ex);        
         } 
    }//GEN-LAST:event_cbotgItemStateChanged

    private void cbonhaxuatbanItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbonhaxuatbanItemStateChanged
        try {             
                String tennxb=cbonhaxuatban.getSelectedItem().toString();             
                ShowTennxb(tennxb);
         } 
         catch (SQLException ex) {             
             Logger.getLogger(frmsach.class.getName()).log(Level.SEVERE, null, ex);        
         } 
    }//GEN-LAST:event_cbonhaxuatbanItemStateChanged

    private void cbotlItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbotlItemStateChanged
        try {             
                String tenlv=cbotl.getSelectedItem().toString();             
                ShowTenlv(tenlv);
         } 
         catch (SQLException ex) {             
             Logger.getLogger(frmsach.class.getName()).log(Level.SEVERE, null, ex);        
         } 
    }//GEN-LAST:event_cbotlItemStateChanged

    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new frmsach().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(frmsach.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnkluu;
    private javax.swing.JButton btnluu;
    private javax.swing.JButton btnsua;
    private javax.swing.JButton btnthem;
    private javax.swing.JButton btnthoat;
    private javax.swing.JButton btnxoa;
    private javax.swing.JComboBox<String> cbonhaxuatban;
    private javax.swing.JComboBox<String> cbotg;
    private javax.swing.JComboBox<String> cbotl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblsach;
    private javax.swing.JTextField txtmasach;
    private javax.swing.JTextField txtnhaxuatban;
    private javax.swing.JTextField txtsoluong;
    private javax.swing.JTextField txttacgia;
    private javax.swing.JTextField txttensach;
    private javax.swing.JTextField txtthloai;
    // End of variables declaration//GEN-END:variables
}
