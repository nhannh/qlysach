
package Interface;
import java.sql.*;
import javax.swing.*;
import Process.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;







public final class frmmuontra extends javax.swing.JFrame {
    private final sach ms = new sach();
    private final thanhvien tv = new thanhvien();
    private final muontra mt = new muontra();
    public static final String FONT = "src/font/times.ttf";
    
    private final DefaultTableModel tableModel = new DefaultTableModel();
    private boolean cothem = true;
    public void showdata() throws  SQLException{
        ResultSet result = mt.xemmt();
        try {
            while (result.next()) {
                String row[] =new String[6];
                row[0] = result.getString(1);
                row[1] = result.getString(2);
                row[2] = result.getString(3);
                row[3] = result.getString(4);
                row[4] = result.getString(5);
                row[5] = result.getString(6);
             
                tableModel.addRow(row);
                
            }
            
        } catch (SQLException e) {
        }
    }
    public final void ShowDataCombos() {         
        ResultSet result=null;                   
        try { 
                result = ms.xemsach();                        
                while(result.next()){  
                cbotg.addItem(result.getString("tens")); 
            } 
        }   
        catch (SQLException e) { 
        }
    }
    public final void ShowDataCombotv() {         
        ResultSet result=null;                   
        try { 
                result = tv.xemtv();                        
                while(result.next()){  
                cbonhaxuatban.addItem(result.getString("tentv")); 
            } 
        }   
        catch (SQLException e) { 
        }
    }
 /*   public final void ShowDataCombotg() {         
        ResultSet result=null;                   
        try { 
                result = tg.xemtg();                        
                while(result.next()){  
                cbotg.addItem(result.getString("tentg")); 
            } 
        }   
        catch (SQLException e) { 
        }
    }*/
    public void ShowTentg(String tentg) throws SQLException
    {                 
        ResultSet result =  ms.laycbms(tentg);
        if(result.next()){ 
            // nếu còn đọc tiếp được một dòng dữ liệu            
        txttacgia.setText(result.getString("mas")); 
        }         
    }   
    public void ShowTennxb(String tennxb) throws SQLException
    {                 
        ResultSet result =  tv.laycbtv(tennxb);
        if(result.next()){ 
            // nếu còn đọc tiếp được một dòng dữ liệu            
        txtnhaxuatban.setText(result.getString("matv")); 
        }         
    }   
   /* public void ShowTenlv(String tenlv) throws SQLException
    {                 
        ResultSet result =  lv.xemlv(tenlv);
        if(result.next()){ 
            // nếu còn đọc tiếp được một dòng dữ liệu            
        txtthloai.setText(result.getString("malv")); 
        }         
    }   
    */
    
    public final void timkiemnhieu(String ten) throws SQLException {        
        ResultSet result=null;          
        result = mt.timnhieutv(ten);
        try {  
            cleardata();
            while(result.next()){ 
                String rows[] = new String[6];
                rows[0] = result.getString(1); 
                rows[1] = result.getString(2); 
                rows[2] = result.getString(3); 
                rows[3] = result.getString(4); 
                rows[4] = result.getString(5); 
                rows[5] = result.getString(6); 
                          
                tableModel.addRow(rows);
            }
        } 
        catch (SQLException e) {
        } 
    }
    
    public void cleardata() throws SQLException{
        int n = tableModel.getRowCount()-1;
        for (int i = n; i >=0; i--) {
            tableModel.removeRow(i);        
        }        
    }
    private void setnull(){   
        txtmasach.setText(null);
        txtnhaxuatban.setText(null);
        //.setText(null);
        txttacgia.setText(null);
      //  txttensach.setText(null);
      //  txtthloai.setText(null);
    //    cbonhaxuatban.setSelectedItem("");
      //  cbotg.setSelectedItem(null);
    //    cbotl.setSelectedItem(null);
    calender.setText(null);
    calender1.setText(null);
    calender2.setText(null);
    
        
        
    }
    private void setkhoa(boolean a){
        txtmasach.setEnabled(!a);
        txtnhaxuatban.setEnabled(!a);
        calender.setEnabled(!a);
        txttacgia.setEnabled(!a);
        calender1.setEnabled(!a);
        calender2.setEnabled(!a);
        cbonhaxuatban.setEnabled(!a);
        cbotg.setEnabled(!a);
        cbonhaxuatban.setEnabled(!a);
    }
    private void setbutton(boolean a){
        btnthem.setEnabled(a);
        btnxoa.setEnabled(a);
        btnsua.setEnabled(a);
        btnluu.setEnabled(!a);
        btnkluu.setEnabled(!a);
        
        
    }
    
    public frmmuontra() throws SQLException {
        initComponents();
        String []colname = {"Mã phiếu","Tên sách","Tên thành viên","Ngày mượn","Hạn trả","Ngày trả"};
        tableModel.setColumnIdentifiers(colname);
        tblsach.setModel(tableModel);
        tblsach.getColumnModel().getColumn(0).setMaxWidth(200);
        showdata();
        ShowDataCombos();
        ShowDataCombotv();
        
        setnull();
        setkhoa(true);
        setbutton(true);
    
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtmasach = new javax.swing.JTextField();
        cbotg = new javax.swing.JComboBox<>();
        txttacgia = new javax.swing.JTextField();
        cbonhaxuatban = new javax.swing.JComboBox<>();
        txtnhaxuatban = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblsach = new javax.swing.JTable();
        btnthem = new javax.swing.JButton();
        btnxoa = new javax.swing.JButton();
        btnsua = new javax.swing.JButton();
        btnluu = new javax.swing.JButton();
        btnkluu = new javax.swing.JButton();
        btnthoat = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jTextField1 = new javax.swing.JTextField();
        txttim = new javax.swing.JButton();
        btntra = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        calender = new javax.swing.JTextField();
        calender1 = new javax.swing.JTextField();
        calender2 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Mượn trả sách");

        jPanel1.setBackground(new java.awt.Color(34, 167, 240));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("QUẢN LÝ MƯỢN TRẢ SÁCH");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(368, 368, 368))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(589, 589, 589))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Thông tin mượn sách"));

        jLabel2.setText("Mã phiếu:");

        jLabel4.setText("Ngày mượn:");

        jLabel5.setText("Sách mượn:");

        jLabel6.setText("Thành viên:");

        txtmasach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmasachActionPerformed(evt);
            }
        });

        cbotg.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbotgItemStateChanged(evt);
            }
        });

        cbonhaxuatban.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        cbonhaxuatban.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbonhaxuatbanItemStateChanged(evt);
            }
        });

        jLabel7.setText("Hạn trả:");

        tblsach.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Mã sách", "Tên sách", "Mã nxb", "Tên NXB", "Mã TG", "Tên TG", "Mã TL", "Tên TL", "Số lượng"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, false, true, false, true, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblsach.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblsachMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblsach);

        btnthem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Add.png"))); // NOI18N
        btnthem.setText("Thêm");
        btnthem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnthemActionPerformed(evt);
            }
        });

        btnxoa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Delete.png"))); // NOI18N
        btnxoa.setText("Xóa");
        btnxoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnxoaActionPerformed(evt);
            }
        });

        btnsua.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Edit.png"))); // NOI18N
        btnsua.setText("Sửa");
        btnsua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsuaActionPerformed(evt);
            }
        });

        btnluu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Save.png"))); // NOI18N
        btnluu.setText("Lưu");
        btnluu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnluuActionPerformed(evt);
            }
        });

        btnkluu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/reload.png"))); // NOI18N
        btnkluu.setText("K.lưu");
        btnkluu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnkluuActionPerformed(evt);
            }
        });

        btnthoat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Exit.png"))); // NOI18N
        btnthoat.setText("Thoát");
        btnthoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnthoatActionPerformed(evt);
            }
        });

        jLabel8.setText("Ngày trả:");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Trả sách", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 12))); // NOI18N

        txttim.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search.png"))); // NOI18N
        txttim.setText("Tìm");
        txttim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttimActionPerformed(evt);
            }
        });

        btntra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/book-icon.png"))); // NOI18N
        btntra.setText("Trả sách");
        btntra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntraActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel3.setText("Nhập thông tin cần tìm:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTextField1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txttim, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(jLabel3)
                .addGap(0, 68, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btntra)
                .addGap(65, 65, 65))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(15, 15, 15)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txttim, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addComponent(btntra)
                .addGap(21, 21, 21))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cbotg, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(calender)
                            .addComponent(calender1)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtmasach, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 59, Short.MAX_VALUE))
                            .addComponent(calender2)
                            .addComponent(cbonhaxuatban, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txttacgia, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                            .addComponent(txtnhaxuatban)))
                    .addComponent(jLabel8)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(60, 60, 60)
                .addComponent(btnthem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnxoa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnsua)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnluu)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnkluu)
                .addGap(18, 18, 18)
                .addComponent(btnthoat)
                .addContainerGap(84, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                .addGap(62, 62, 62)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnthem)
                    .addComponent(btnxoa)
                    .addComponent(btnsua)
                    .addComponent(btnluu)
                    .addComponent(btnkluu)
                    .addComponent(btnthoat))
                .addGap(97, 97, 97))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtmasach, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cbotg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttacgia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(cbonhaxuatban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnhaxuatban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(calender, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(calender1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(calender2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.getAccessibleContext().setAccessibleName("Thông tin phiếu mượn");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void tblsachMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblsachMouseClicked
         try {
            int row = tblsach.getSelectedRow();
            String mas = (tblsach.getModel().getValueAt(row, 0)).toString();
            ResultSet rs = mt.xemmt(mas);
            if (rs.next()) {
                txtmasach.setText(rs.getString("sophieu"));
                txtnhaxuatban.setText(rs.getString("matv"));
                txttacgia.setText(rs.getString("mas"));
                cbonhaxuatban.setSelectedItem(rs.getString("tentv"));
                cbotg.setSelectedItem(rs.getString("tens"));
               
//                java.util.Date date = new SimpleDateFormat().parse(rs.getString("ngaymuon");
//                java.util.Date date1 = new SimpleDateFormat().parse(rs.getString("hantra"));
//                java.util.Date date2 = new SimpleDateFormat().parse(rs.getString("ngaytra"));
//                calender.setDate(date);
//                calender1.setDate(date1);
//                calender3.setDate(date2);
                calender.setText(rs.getString("ngaymuon"));
                calender1.setText(rs.getString("hantra"));
                calender2.setText(rs.getString("ngaytra"));
 
            }
            
        } catch (SQLException e) {
        }
    }//GEN-LAST:event_tblsachMouseClicked

    private void btnthemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnthemActionPerformed
        setnull();
        setbutton(false);
        setkhoa(false);
        cothem =true;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        java.util.Date date = cal.getTime();
        String a = sdf.format(date);
        calender.setText(a);
    }//GEN-LAST:event_btnthemActionPerformed

    private void btnxoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnxoaActionPerformed
        String msv = txtmasach.getText();
        
        try {
            if (msv.length()==0) {
                JOptionPane.showMessageDialog(null, "Chọn 1 sản phẩm để xóa", "Thông báo", 1);
            }
            else {
                if(JOptionPane.showConfirmDialog(null,"Bạn có chắc chắn muốn xóa hay không?" ,"Thông báo",2)==0)
                {
                    mt.xoamt(msv);
                    cleardata();
                    showdata();
                    setnull();
                }
            }       
        } catch (SQLException e) {
        }
    }//GEN-LAST:event_btnxoaActionPerformed

    private void btnsuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsuaActionPerformed
        String mas = txtmasach.getText();
        if (mas.length()==0) {
            JOptionPane.showMessageDialog(null, "Chọn mã sách cần sửa","Thông báo",1);
        }
        else{
            setkhoa(false);
            txtmasach.setEnabled(false);
            setbutton(false);
            cothem = false;
        }
                      
    }//GEN-LAST:event_btnsuaActionPerformed

    private void btnluuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnluuActionPerformed
        String ma=txtmasach.getText(); 
        String matg=txttacgia.getText(); 
        String manxb=txtnhaxuatban.getText();    
        String ngaymuon= calender.getText();
        String hantra= calender1.getText();
        String ngaytra= calender2.getText();
        
        if(ma.length()==0)              
                JOptionPane.showMessageDialog(null,"Vui long nhap Ma Sach va ten sach", "Thong bao",1);         
        else 
         //   if(ma.length()>0 || ten.length()>0)              
             //   JOptionPane.showMessageDialog(null,"Ma SP chi 2 ky tu, ten SP la 30", "Thong bao",1);             
        //else                
        {               
            try 
            { 
                if(cothem==true)    
                //Luu cho tthem moi      
                  
                mt.themmt(ma, manxb, matg, ngaymuon, hantra, ngaytra);
                else                
                //Luu cho sua 
                mt.suamt(ma, manxb, matg, ngaymuon, hantra, ngaytra);
                cleardata();
                showdata();
            } 
                catch (SQLException ex) 
                { 
                   JOptionPane.showMessageDialog(null,"Cap nhat that bai","Thong bao",1); 
                }                         
                    setnull();
                    setkhoa(true);             
                    setbutton(true); 
        }

    }//GEN-LAST:event_btnluuActionPerformed

    private void btnkluuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnkluuActionPerformed
        setnull();
        setkhoa(true);
        setbutton(true);
    }//GEN-LAST:event_btnkluuActionPerformed

    private void btnthoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnthoatActionPerformed
             this.dispose();
        frmMain f = new frmMain();
        f.setVisible(true);
    }//GEN-LAST:event_btnthoatActionPerformed

    private void cbotgItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbotgItemStateChanged
   
        try {             
                String tentg=cbotg.getSelectedItem().toString();             
                ShowTentg(tentg);
         } 
         catch (SQLException ex) {             
             Logger.getLogger(frmmuontra.class.getName()).log(Level.SEVERE, null, ex);        
         } 
    }//GEN-LAST:event_cbotgItemStateChanged

    private void cbonhaxuatbanItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbonhaxuatbanItemStateChanged
        try {             
                String tennxb=cbonhaxuatban.getSelectedItem().toString();             
                ShowTennxb(tennxb);
         } 
         catch (SQLException ex) {             
             Logger.getLogger(frmmuontra.class.getName()).log(Level.SEVERE, null, ex);        
         } 
    }//GEN-LAST:event_cbonhaxuatbanItemStateChanged

    private void txtmasachActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmasachActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmasachActionPerformed

    private void btntraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntraActionPerformed
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        java.util.Date date = cal.getTime();
        String a = sdf.format(date);
        String b = txtmasach.getText();
        String ngaytra= calender2.getText();
        

        if(b.equals("")){              
                JOptionPane.showMessageDialog(null,"Bạn phải chọn người mượn","Thông báo",1);
        }
        else 
        {
            if ("".equals(ngaytra)) {
                try {

                    mt.trasach(a, b);
                    cleardata();
                    showdata();
                } 
                catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Cap nhat that bai", "Thong bao", 1);
                }
                setnull();
                setkhoa(true);
                setbutton(true);
            
            }    
            else {
                JOptionPane.showMessageDialog(null,"Người mượn đã trả sách rồi","Thông báo", 1);
            }
        }

    }//GEN-LAST:event_btntraActionPerformed

    private void txttimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttimActionPerformed
        String tim = jTextField1.getText();
        try {
            timkiemnhieu(tim);
        } catch (SQLException ex) {
            Logger.getLogger(frmmuontra.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_txttimActionPerformed
   

    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new frmmuontra().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(frmmuontra.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnkluu;
    private javax.swing.JButton btnluu;
    private javax.swing.JButton btnsua;
    private javax.swing.JButton btnthem;
    private javax.swing.JButton btnthoat;
    private javax.swing.JButton btntra;
    private javax.swing.JButton btnxoa;
    private javax.swing.JTextField calender;
    private javax.swing.JTextField calender1;
    private javax.swing.JTextField calender2;
    private javax.swing.JComboBox<String> cbonhaxuatban;
    private javax.swing.JComboBox<String> cbotg;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTable tblsach;
    private javax.swing.JTextField txtmasach;
    private javax.swing.JTextField txtnhaxuatban;
    private javax.swing.JTextField txttacgia;
    private javax.swing.JButton txttim;
    // End of variables declaration//GEN-END:variables
}
