
package Interface;
import java.sql.*;
import javax.swing.*;
import Process.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;



public final class frmtim extends javax.swing.JFrame {
    private final sach ms = new sach();
    private final nxb nhaxb = new nxb();
    private final linhvuc lv = new linhvuc();
    private final tacgia tg = new tacgia();
    private final DefaultTableModel tableModel = new DefaultTableModel();
    private boolean cothem = true;
    public void showdata() throws  SQLException{
        ResultSet result = ms.xemsach();
        try {
            while (result.next()) {
                String row[] =new String[6];
                row[0] = result.getString(1);
                row[1] = result.getString(2);
                row[2] = result.getString(4);
                row[3] = result.getString(6);
                row[4] = result.getString(8);
                row[5] = result.getString(9);
             
                tableModel.addRow(row);
                
            }
            
        } catch (SQLException e) {
        }
    }
//    public final void ShowDataCombonxb() {         
//        ResultSet result=null;                   
//        try { 
//                result = nhaxb.xemnxb();                        
//                while(result.next()){  
//                cbonhaxuatban.addItem(result.getString("tennxb")); 
//            } 
//        }   
//        catch (SQLException e) { 
//        }
//    }
//    public final void ShowDataCombolv() {         
//        ResultSet result=null;                   
//        try { 
//                result = lv.xemlv();                        
//                while(result.next()){  
//                cbotl.addItem(result.getString("tenlv")); 
//            } 
//        }   
//        catch (SQLException e) { 
//        }
//    }
//    public final void ShowDataCombotg() {         
//        ResultSet result=null;                   
//        try { 
//                result = tg.xemtg();                        
//                while(result.next()){  
//                cbotg.addItem(result.getString("tentg")); 
//            } 
//        }   
//        catch (SQLException e) { 
//        }
//    }
//    public void ShowTentg(String tentg) throws SQLException
//    {                 
//        ResultSet result =  tg.xemtg(tentg);           
//        if(result.next()){ 
//            // nếu còn đọc tiếp được một dòng dữ liệu            
//        txttacgia.setText(result.getString("matg")); 
//        }         
//    }   
//    public void ShowTennxb(String tennxb) throws SQLException
//    {                 
//        ResultSet result =  nhaxb.xemnxb(tennxb);
//        if(result.next()){ 
//            // nếu còn đọc tiếp được một dòng dữ liệu            
//        txtnhaxuatban.setText(result.getString("manxb")); 
//        }         
//    }   
//    public void ShowTenlv(String tenlv) throws SQLException
//    {                 
//        ResultSet result =  lv.xemlv(tenlv);
//        if(result.next()){ 
//            // nếu còn đọc tiếp được một dòng dữ liệu            
//        txtthloai.setText(result.getString("malv")); 
//        }         
//    }   
    
    public void cleardata() throws SQLException{
        int n = tableModel.getRowCount()-1;
        for (int i = n; i >=0; i--) {
            tableModel.removeRow(i);        
        }        
    }
    
    public final void timkiem(String muctimkiem,String ten) throws SQLException {        
        ResultSet result=null;          
        result = ms.tims(muctimkiem, ten);
        try {  
            cleardata();
            while(result.next()){ 
                String rows[] = new String[6];
                rows[0] = result.getString(1); 
                rows[1] = result.getString(2); 
                rows[2] = result.getString(4); 
                rows[3] = result.getString(6); 
                rows[4] = result.getString(8); 
                rows[5] = result.getString(9); 
                          
                tableModel.addRow(rows);
            }
        } 
        catch (SQLException e) {
        } 
    }
    public final void timkiemnhieu(String ten) throws SQLException {        
        ResultSet result=null;          
        result = ms.timnhieus(ten);
        try {  
            cleardata();
            while(result.next()){ 
                String rows[] = new String[6];
                rows[0] = result.getString(1); 
                rows[1] = result.getString(2); 
                rows[2] = result.getString(4); 
                rows[3] = result.getString(6); 
                rows[4] = result.getString(8); 
                rows[5] = result.getString(9); 
                          
                tableModel.addRow(rows);
            }
        } 
        catch (SQLException e) {
        } 
    }
//    private void setnull(){   
//        txtmasach.setText(null);
//        txtnhaxuatban.setText(null);
//        txtsoluong.setText(null);
//        txttacgia.setText(null);
//        txttensach.setText(null);
//        txtthloai.setText(null);
//    //    cbonhaxuatban.setSelectedItem("");
//      //  cbotg.setSelectedItem(null);
//    //    cbotl.setSelectedItem(null);
//        
//    }
//    private void setkhoa(boolean a){
//        txtmasach.setEnabled(!a);
//        txtnhaxuatban.setEnabled(!a);
//        txtsoluong.setEnabled(!a);
//        txttacgia.setEnabled(!a);
//        txttensach.setEnabled(!a);
//        txtthloai.setEnabled(!a);
//        cbonhaxuatban.setEnabled(!a);
//        cbotg.setEnabled(!a);
//        cbotl.setEnabled(!a);
//    }
//    private void setbutton(boolean a){
//        btnthem.setEnabled(a);
//        btnxoa.setEnabled(a);
//        btnsua.setEnabled(a);
//        btnluu.setEnabled(!a);
//        btnkluu.setEnabled(!a);
//        
//        
//    }
//    
    public frmtim() throws SQLException {
        initComponents();
        String []colname = {"Mã sách","Tên sách","Tên tác giả","Tên nhà xuất bản","Tên thể loại","Số lượng"};
        tableModel.setColumnIdentifiers(colname);
        tblsach.setModel(tableModel);
        showdata();
//        ShowDataCombonxb();
//        ShowDataCombolv();
//        ShowDataCombotg();
//        setnull();
//        setkhoa(true);
//        setbutton(true);
    
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblsach = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        txttim = new javax.swing.JTextField();
        btntim = new javax.swing.JButton();
        rdsach = new javax.swing.JRadioButton();
        rdtg = new javax.swing.JRadioButton();
        rdnxb = new javax.swing.JRadioButton();
        rdtl = new javax.swing.JRadioButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Danh mục sách");

        jPanel1.setBackground(new java.awt.Color(34, 167, 240));

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("QUẢN LÝ TÌM KIẾM SÁCH");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(275, 275, 275)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(589, 589, 589))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 204));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tra cứu sách", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 14))); // NOI18N

        tblsach.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Mã sách", "Tên sách", "Mã nxb", "Tên NXB", "Mã TG", "Tên TG", "Mã TL", "Tên TL", "Số lượng"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, false, true, false, true, false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblsach);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Mục tìm kiếm", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        btntim.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btntim.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Search.png"))); // NOI18N
        btntim.setText("Tìm");
        btntim.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btntimActionPerformed(evt);
            }
        });

        buttonGroup1.add(rdsach);
        rdsach.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        rdsach.setText("Tên sách");

        buttonGroup1.add(rdtg);
        rdtg.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        rdtg.setText("Tác giả");

        buttonGroup1.add(rdnxb);
        rdnxb.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        rdnxb.setText("Nhà xuất bản");

        buttonGroup1.add(rdtl);
        rdtl.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        rdtl.setText("Thể loại");

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/reload.png"))); // NOI18N
        jButton1.setText("Xóa");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/Exit.png"))); // NOI18N
        jButton2.setText("Thoát");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txttim, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(btntim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addGap(21, 21, 21))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(rdsach)
                        .addGap(18, 18, 18)
                        .addComponent(rdtg)
                        .addGap(18, 18, 18)
                        .addComponent(rdnxb)
                        .addGap(10, 10, 10)
                        .addComponent(rdtl))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(jButton2)))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btntim)
                    .addComponent(txttim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdsach)
                    .addComponent(rdtg)
                    .addComponent(rdnxb)
                    .addComponent(rdtl))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(jButton2))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 738, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(77, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btntimActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btntimActionPerformed
        String tim = txttim.getText();
        String muctimkiem = null;
        if (!rdnxb.isSelected() && !rdsach.isSelected()&& !rdtg.isSelected()&& !rdtl.isSelected()) {
            try {
                timkiemnhieu(tim);
            } catch (SQLException ex) {
                Logger.getLogger(frmtim.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            if (rdsach.isSelected()) {

                muctimkiem = "tens";
                try {
                    timkiem(muctimkiem, tim);
                } catch (SQLException ex) {
                    Logger.getLogger(frmtim.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (rdtg.isSelected()) {
                muctimkiem = "tentg";
                try {
                    timkiem(muctimkiem, tim);
                } catch (SQLException ex) {
                    Logger.getLogger(frmtim.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            if (rdnxb.isSelected()) {
                muctimkiem = "tennxb";
                try {
                    timkiem(muctimkiem, tim);
                } catch (SQLException ex) {
                    Logger.getLogger(frmtim.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            if (rdtl.isSelected()) {
                muctimkiem = "tenlv";
                try {
                    timkiem(muctimkiem, tim);
                } catch (SQLException ex) {
                    Logger.getLogger(frmtim.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
    }//GEN-LAST:event_btntimActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        buttonGroup1.clearSelection();
        try {
            cleardata();
            showdata();
            txttim.setText(null);
        } catch (SQLException ex) {
            Logger.getLogger(frmtim.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if (frmLogin.co ==0) {
            this.dispose();
            frmMain f = new frmMain();
            f.setVisible(true);}
        else {
            this.dispose();
            frmLogin f = new frmLogin();
            f.setVisible(true);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new frmtim().setVisible(true);
                } catch (SQLException ex) {
                    Logger.getLogger(frmtim.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btntim;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton rdnxb;
    private javax.swing.JRadioButton rdsach;
    private javax.swing.JRadioButton rdtg;
    private javax.swing.JRadioButton rdtl;
    private javax.swing.JTable tblsach;
    private javax.swing.JTextField txttim;
    // End of variables declaration//GEN-END:variables
}
